<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class mahasiswaController extends Controller
{
    public function create(){
        return view('mahasiswa.create');
    }

    public function store(Request $request){
        $validatedData = $request->validate([
            'nama' => 'required|unique:mahasiswa,nama|max:255',
            'angkatan' => 'required',
            'departemen' => 'required',
            'jabatan' => 'required',
        ],
        [
            'nama.required' => 'Isi nama mahasiswa',
            'angkatan.required' => 'Isi angkatan mahasiswa',
            'departemen.required' => 'Isi departemen mahasiswa',
            'jabatan.required' => 'Isi jabatan mahasiswa', 
        ]);    

        DB::table('mahasiswa')->insert(
            [
                'nama' => $request['nama'],
                'angkatan' => $request['angkatan'],
                'departemen' => $request['departemen'],
                'jabatan' => $request['jabatan'],
            ]
        );

        return redirect('/mahasiswa'); //berfungsi sebagai pengembali ke halaman create setelah mengisi form
    }

    public function index()
    {
        $mahasiswa = DB::table('mahasiswa')->get();

        return view('mahasiswa.index', ['mahasiswa' => $mahasiswa]);
    }

    public function show($id)
    {
        $mahasiswa = DB::table('mahasiswa')->where('id', $id)->first();
        return view('mahasiswa.show', compact('mahasiswa'));
    }
   
    public function edit($id)
    {
        $mahasiswa = DB::table('mahasiswa')->where('id', $id)->first();
        return view('mahasiswa.edit', compact('mahasiswa'));

    }
        public function update($id, Request $request)
    {
        $request->validate([
            'nama' => 'required',
            'angkatan' => 'required',
            'departemen' => 'required',
            'jabatan' => 'required',
        ],
        [
            'nama.required' => 'Inputan nama tidak boleh kosong',
            'angkatan.required' => 'Inputan umur tidak boleh kosong',
            'departemen.required' => 'Inputan bio tidak boleh kosong',
            'jabatan.required' => 'Inputan bio tidak boleh kosong', 
        ]
        );

        $query = DB::table('mahasiswa')
            ->where('id', $id)
            ->update([
                'nama' => $request['nama'],
                'angkatan' => $request['angkatan'],
                'departemen' => $request['departemen'],
                'jabatan' => $request['jabatan'],
            ]);
        return redirect('/mahasiswa');
    }

    public function destroy($id){
        $query = DB::table('mahasiswa')->where('id', $id)->delete();

        return redirect('/mahasiswa')->with('success', 'Nama anggota berhasil dihapus!');
    }

}
