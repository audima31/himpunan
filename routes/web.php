<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('layouts.master');
});

Route::group(['middleware' => ['auth']], function () {
    // CRUD Mahasiswa
    Route::get('/mahasiswa/create','mahasiswaController@create');
    Route::post('/mahasiswa','mahasiswaController@store');
    Route::get('/mahasiswa','mahasiswaController@index');
    Route::get('/mahasiswa/{id}','mahasiswaController@show');
    Route::get('/mahasiswa/{id}/edit','mahasiswaController@edit');
    Route::put('/mahasiswa/{id}','mahasiswaController@update');
    Route::delete('/mahasiswa/{id}','mahasiswaController@destroy');
    Route::get('/info', function () {
        return view('mahasiswa.info');
    });

    // CRUD Profile
    Route::resource('profile', 'ProfileController')->only([
        'index', 'update'
    ]);
});

Auth::routes();

