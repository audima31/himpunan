<!DOCTYPE html>
<html lang="en" class="h-100">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <title>HIMA</title>
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="{{asset('admin/images/favicon.png')}}">
    <link href="{{asset('admin/css/style.css')}}" rel="stylesheet">

</head>

<body class="h-100">
    <div class="authincation h-100">
        <div class="container-fluid h-100">
            <div class="row justify-content-center h-100 align-items-center">
                <div class="col-md-6">
                    <div class="authincation-content">
                        <div class="row no-gutters">
                            <div class="col-xl-12">
                                <div class="auth-form">
                                    <h4 class="text-center mb-4">Sign up your account</h4>
                                    
                                    <form action="{{ route('register') }}" method="post">
                                        @csrf
                                        <div class="input-group mb-3">
                                          <input type="text" name="name" class="form-control" placeholder="Full name">
                                          <div class="input-group-append">
                                          </div>
                                        </div>
                                        @error('name')
                                            <div class="alert alert-danger">
                                              {{ $message }}
                                            </div>
                                        @enderror
                                        <div class="input-group mb-3">
                                          <input type="email" name="email" class="form-control" placeholder="Email">
                                        </div>
                                        @error('email')
                                            <div class="alert alert-danger">
                                              {{ $message }}
                                            </div>
                                        @enderror
                                        <div class="input-group mb-3">
                                          <input type="password" name="password" class="form-control" placeholder="Password">
                                        </div>
                                        @error('password')
                                        <div class="alert alert-danger">
                                          {{ $message }}
                                        </div>
                                        @enderror
                                        <div class="input-group mb-3">
                                          <input type="password" name="password_confirmation" class="form-control" placeholder="Retype password">
                                        </div>
                                        <div class="input-group mb-3">
                                          <input type="number" name="NIM" class="form-control" placeholder="NIM">
                                        </div>
                                        @error('NIM')
                                            <div class="alert alert-danger">
                                              {{ $message }}
                                            </div>
                                        @enderror
                                        <div class="input-group mb-3">
                                            <input type="text" name="jurusan" class="form-control" placeholder="Jurusan">
                                        </div>
                                        @error('jurusan')
                                              <div class="alert alert-danger">
                                                {{ $message }}
                                              </div>
                                        @enderror
                                        
                                        <div class="text-center mt-4">
                                            <button type="submit" class="btn btn-primary btn-block">Sign me up</button>
                                        </div>
                                    </form>
                                    <div class="new-account mt-3">
                                        <p>Already have an account? <a class="text-primary" href="/login">Sign in</a></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!--**********************************
        Scripts
    ***********************************-->
    <!-- Required vendors -->
    <script src="{{asset('admin/vendor/global/global.min.js')}}"></script>
    <script src="admin/js/quixnav-init.js')}}"></script>
    <!--endRemoveIf(production)-->
</body>

</html>