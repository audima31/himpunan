@extends('layouts.master')

@section('title')
Daftar Mahasiswa
@endsection

@section('route-1')
    Mahasiswa
@endsection

@section('route-2')
    Daftar Mahasiswa
@endsection

@section('konten')
<a href="/mahasiswa/create" class="btn btn-success btn-sm my-2">Tambah Anggota</a>
<a href="/info" class="btn btn-success btn-sm my-2">Info Department</a>

<table class="table table-striped">
    <thead>
      <tr>
        <th scope="col">#</th>
        <th scope="col">Nama</th>
        <th scope="col">Angkatan</th>
        <th scope="col">Departemen</th>
        <th scope="col">Jabatan</th>
      </tr>
    </thead>
    <tbody>
        @forelse ($mahasiswa as $key => $item) <!--Looping data-->
            <tr>
                <td> {{$key + 1}}</td>
                <td>{{$item->nama}}</td>
                <td>{{$item->angkatan}}</td>
                <td>{{$item->departemen}}</td>
                <td>{{$item->jabatan}}</td>
                <td>
                    <form action="/mahasiswa/{{$item->id}}" method="POST">
                    @csrf
                    @method('DELETE')
                    <a href="/mahasiswa/{{$item->id}}" class="btn btn-primary btn-sm">Detail</a>
                    <a href="/mahasiswa/{{$item->id}}/edit" class="btn btn-secondary btn-sm">Edit</a>
                    <input type="submit" value="delete" class="btn btn-danger btn-sm">
                    </form>
                </td>
            </tr>
        @empty
            <tr>
                <td>Tidak ada Data</td>
            </tr>
        @endforelse
    </tbody>
  </table>

@endsection

