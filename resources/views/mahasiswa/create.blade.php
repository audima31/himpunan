@extends('layouts.master')

@section('title')   
<h2>Tambah Anggota HIMPUNAN</h2>
@endsection

@section('route-1')
    Mahasiswa
@endsection

@section('route-2')
    Create
@endsection

@section('konten')
<div>
        <form action="/mahasiswa" method="POST">
            @csrf
            <div class="form-group">
                <label for="title">Nama</label>
                <input type="text" class="form-control" name="nama" id="nama" placeholder="Masukkan Title">
                @error('nama')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="title">Angkatan</label>
                <input type="text" class="form-control" name="angkatan" id="angkatan" placeholder="Masukkan Title">
                @error('angkatan')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>

            <div class="form-group">
                <label for="title">Departemen</label>
                <input type="text" class="form-control" name="departemen" id="departemen" placeholder="Masukkan Title">
                @error('departemen')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>

            <div class="form-group">
                <label for="title">Jabatan</label>
                <input type="text" class="form-control" name="jabatan" id="jabatan" placeholder="Masukkan Title">
                @error('jabatan')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>


            
            <button type="submit" class="btn btn-primary">Tambah</button>
        </form>
</div>
@endsection