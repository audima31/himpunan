<!--**********************************
    Sidebar start
***********************************-->
<div class="quixnav">
            <div class="quixnav-scroll">
                <ul class="metismenu" id="menu">
                    <li class="nav-label first"></li>
                    <!-- <li><a href="index.html"><i class="icon icon-single-04"></i><span class="nav-text">Dashboard</span></a>
                    </li> -->
                    @auth
                        <li><a class="has-arrow" href="javascript:void()" aria-expanded="false"><i
                                    class="icon icon-single-04"></i><span class="nav-text">Profile</span></a>
                            <ul aria-expanded="false">
                                <li><a href="/profile">Profile</a></li>
                            </ul>
                            <ul aria-expanded="false">
                                <li><a href="/edit-profile">Edit Profile</a></li>
                            </ul>
                            <ul class="nav-item">
                                <a class="nav-link" href="{{ route('logout') }}"      
                                onclick="event.preventDefault();
                                document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                </a>
                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    @csrf
                                </form>
                                </ul>
                            
                        </li>  
                    @endauth
                    
                    <li><a class="has-arrow" href="javascript:void()" aria-expanded="false"><i
                                class="icon icon-world-2"></i><span class="nav-text">Mahasiswa</span></a>
                        <ul aria-expanded="false">
                            <li><a href="/mahasiswa">Daftar Mahasiswa</a></li>
                        </ul>
                        <ul aria-expanded="false">
                            <li><a href="/mahasiswa/create">Create</a></li>
                        </ul>
                    </li>
                    @guest
                        <li class="nav-item">
                            <a href="/login" class="nav-link bg-green">
                            <p>
                            Login
                            </p>
                            </a>
                        </li>
                    @endguest
                    

                </ul>
            </div>
        </div>
<!--**********************************
    Sidebar end
***********************************-->